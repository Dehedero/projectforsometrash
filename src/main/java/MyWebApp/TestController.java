package MyWebApp;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/test")
public class TestController {

    @GetMapping
    public String test(){
        String var = System.getenv("MY_VAR");
        return var + " this is test method!\n" + new Date().toString();
    }
}
